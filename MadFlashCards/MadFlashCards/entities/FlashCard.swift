//
//  FlashCard.swift
//  flashcards
//
//  Created by Student Account  on 1/21/22.
//

import Foundation

class FlashCard{
    var title:String
    var defintion: String
    
    init(title: String, defintion: String ){
            self.title = title
            self.defintion = defintion
            
        }

    
    func returningFlashCards() -> Array<FlashCard>{
        var flashCard1 = FlashCard(title: "1+1",defintion: "2")
        var flashCard2 = FlashCard(title: "1+2",defintion: "3")
        var flashCard3 = FlashCard(title: "1+3",defintion: "4")
        var flashCard4 = FlashCard(title: "1+4",defintion: "5")
        var flashCard5 = FlashCard(title: "1+5",defintion: "6")
        var flashCard6 = FlashCard(title: "1+6",defintion: "7")
        var flashCard7 = FlashCard(title: "1+7",defintion: "8")
        var flashCard8 = FlashCard(title: "1+8",defintion: "9")
        var flashCard9 = FlashCard(title: "1+9",defintion: "10")
        var flashCard10 = FlashCard(title: "1+10",defintion: "11")
        var items = Array<FlashCard>()
        items.append(flashCard1)
        items.append(flashCard2)
        items.append(flashCard3)
        items.append(flashCard4)
        items.append(flashCard5)
        items.append(flashCard6)
        items.append(flashCard7)
        items.append(flashCard8)
        items.append(flashCard9)
        items.append(flashCard10)
        return items
    }
    
}
